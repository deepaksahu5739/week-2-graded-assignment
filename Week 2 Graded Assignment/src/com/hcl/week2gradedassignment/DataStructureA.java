package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	
	public void sortingNames(ArrayList<Employee>employees) {
		
		ArrayList<String>sortingnames = new ArrayList<String>();
		
		for(Employee employe1 : employees) {
			
			sortingnames.add(employe1.getName());
		}
		
		Collections.sort(sortingnames);
		
		System.out.println();
		System.out.println("Name of all the employees in sorted order are:");
		System.out.println(sortingnames);
		
		}

}