package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.TreeMap;

public class DataStructureC {

	public void monthlySalary(ArrayList<Employee> employees) {

		try {

			TreeMap<Integer, Float> monthlysalary = new TreeMap<>();

			for (Employee employe3 : employees) {
				monthlysalary.put(employe3.getId(), (float) Math.floor((float) employe3.getSalary() / 12));
			}

			System.out.println();
			System.out.println("Monthly salary of employee along with their ID is: ");
			System.out.println(monthlysalary);
		}

		catch (Exception exception) {

			System.out.println("exception found " + exception.getMessage());
		}
	}

}