package com.hcl.week2gradedassignment;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		ArrayList<Employee> employees = new ArrayList<>();

		Employee employee1 = new Employee();
		Employee employee2 = new Employee();
		Employee employee3 = new Employee();
		Employee employee4 = new Employee();
		Employee employee5 = new Employee();
		// setting details of emp1

		employee1.setId(1);
		employee1.setName("Aman");
		employee1.setAge(20);
		employee1.setSalary(1100000);
		employee1.setDepartment("IT");
		employee1.setCity("Delhi");

		// setting details of emp2

		employee2.setId(2);
		employee2.setName("Bobby");
		employee2.setAge(22);
		employee2.setSalary(500000);
		employee2.setDepartment("HR");
		employee2.setCity("Bombay");

		// setting details of emp3
		employee3.setId(3);
		employee3.setName("Zoe");
		employee3.setAge(20);
		employee3.setSalary(750000);
		employee3.setDepartment("Admin");
		employee3.setCity("Delhi");

		// setting details of emp4
		employee4.setId(4);
		employee4.setName("Smitha");
		employee4.setAge(21);
		employee4.setSalary(1000000);
		employee4.setDepartment("IT");
		employee4.setCity("Chennai");

		// setting details of emp5
		employee5.setId(5);
		employee5.setName("Smitha");
		employee5.setAge(24);
		employee5.setSalary(1200000);
		employee5.setDepartment("HR");
		employee5.setCity("Bangalore");

		ArrayList<Employee> employee = new ArrayList<Employee>();
		
		employee.add(employee1);
		employee.add(employee2);
		employee.add(employee3);
		employee.add(employee4);
		employee.add(employee5);

		System.out.println("List of Employees : ");
		
		System.out.println("S.No.  Name    Age    Salary(INR)   Department Location");
		
		for (Employee em : employee) {
			System.out.println(em.getId() + "     " + em.getName() + "      " + em.getAge() + "     " + em.getSalary()
					+ "      " + em.getDepartment() + "       " + em.getCity());
		}
		
		System.out.println();
		
		DataStructureA datastructureA = new DataStructureA();
		datastructureA.sortingNames(employee);
		DataStructureB datastructureB = new DataStructureB();
		datastructureB.cityNameCount(employee);
		DataStructureC datastructureC = new DataStructureC();
		datastructureC.monthlySalary(employee);

	}
}
