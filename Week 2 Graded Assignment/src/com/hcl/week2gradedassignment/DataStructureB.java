package com.hcl.week2gradedassignment;

import java.util.ArrayList;
import java.util.TreeMap;

public class DataStructureB {

	public void cityNameCount(ArrayList<Employee> employees) {

		TreeMap<String, Integer> cityNameCount = new TreeMap<>();

		for (Employee employe2 : employees) {

			String city = employe2.getCity();

			if (cityNameCount.containsKey(city)) {
				cityNameCount.replace(city, (((int) cityNameCount.get(city)) + 1));
			} else {
				cityNameCount.put(city, 1);
			}
		}

		System.out.println();
		System.out.println("Count of employees from each city:");
		System.out.println(" " + cityNameCount);

	}
}